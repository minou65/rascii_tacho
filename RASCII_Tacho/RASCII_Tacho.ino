/*
Copyright (c) 2018 andy      <don_funk@bluewin.ch>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created by Andreas Zogg, November 30, 2018.

RASCII_Tacho.ino - Version 1.0.2
*/

// #define FORCE_RESET_FACTORY_DEFAULT_CV

#define DEBUG_INIT
// #define DEBUG_MSG

#include <EEPROM.h>
#include "RASCII.h"
#include "CVStruct.h"


#ifdef DEBUG_INIT
#include <MemoryFree.h>
#endif // DEBUG_MSG

#define LED_PIN 13

#define stat_Run		1
#define stat_Start		2
#define stat_Stop		3
#define stat_Calc		3
#define stat_Idle		4

const float Pie = 3.14159;				// To however many digits you want.

int rpmMAXCount = 0;
volatile int rpmCount = 0;

unsigned long lastmillis = 0;
unsigned long rpmStart_ms = 0;
unsigned long rpmStop_ms = 0;

bool rpmStart = false;
bool rpmStop = false;
bool rpmValues[2] = { 0, 0 };
uint8_t rpmStatus = 0;

uint32_t CV_Pause = 0;				// in sekunden
uint8_t CV_Distance = 0;			// in cm

RASCII rascii(false);

uint8_t myaddr = Default_Addr;

uint8_t GetCV(uint16_t CV_) {
	return EEPROM.read(CV_);
}

bool validCV(uint16_t CV_) {
	bool valid_ = false;
	for (uint8_t i_; i_ < sizeof(DefaultCVs) / sizeof(CVPair); i_++) {
		uint8_t _cv = pgm_read_word(&DefaultCVs[i_].CV);
		if (_cv == CV_) {
			valid_ = true;
		}
	}
#ifdef DEBUG_MSG
	Serial.print(F("CV ")); Serial.print(CV_); Serial.print(F(" is ")); Serial.println(valid_);
#endif 
	return valid_;
};

uint8_t SetCV(uint16_t CV_, uint8_t value_) {
	EEPROM.write(CV_, value_);
	//if ((EEPROM.read(CV_) != value_) && validCV(CV_)) {
	//	EEPROM.write(CV_, value_);
	//	notify_CVChange(CV_, value_);
	//}
	return EEPROM.read(CV_);
}


void initDecoder() {

#ifdef DEBUG_INIT
	Serial.println(F("void::initDecoder"));
#endif	
	rascii.stopRun();

	myaddr = GetCV(CV_Addr);

	float _Diameter = (float)(GetCV(100)) / (float)(100);		// Umrechnen nach cm
	CV_Distance = GetCV(101);				// in cm
	CV_Pause = GetCV(102) * 100;			// in Sekunden
	
	float _Umfang = _Diameter * Pie;

	rpmMAXCount = (int)(CV_Distance / _Umfang);

#ifdef DEBUG_INIT
	uint16_t freeMemory_ = freeMemory();
	Serial.println(F("========================================="));
	Serial.print(F("Init Decoder: Address: ")); Serial.println(GetCV(CV_Addr), DEC);
	Serial.print(F("  Free Memory: ")); Serial.println(freeMemory_);
	Serial.println(F(" "));

	Serial.print(F("  CV Distance (cm): ")); Serial.println(CV_Distance);
	Serial.print(F("  CV Diameter:      ")); Serial.println(GetCV(100));
	Serial.print(F("  Diameter (cm):    ")); Serial.println(_Diameter);
	Serial.print(F("  MAXCount:         ")); Serial.println(rpmMAXCount);
#endif  

	rpmStart = false;
	rpmStop = false;
	rpmStatus = stat_Start;

	rascii.startRun();

};

// the setup function runs once when you press reset or power the board
void setup() {
	pinMode(DD2, INPUT_PULLUP);	// Sensor
	digitalWrite(DD2, HIGH);

	pinMode(DD3, OUTPUT);	// Start
	pinMode(DD4, OUTPUT);	// Stop

	digitalWrite(DD3, LOW);
	digitalWrite(DD4, LOW);

	//Setup the LED :
	pinMode(LED_PIN, OUTPUT);

	// Start RASCII
	rascii.begin(57600);

#if !defined(FORCE_RESET_FACTORY_DEFAULT_CV)
	//if eeprom has 0xFF then assume it needs to be programmed
	if (GetCV(100) == 0xFF) {

#ifdef DEBUG_INIT
		Serial.println("CV Defaulting due to blank eeprom");
#endif

		notify_CMD_RESETCONF();

	}
	else {
#ifdef DEBUG_INIT
		Serial.println("CV Not Defaulting");
#endif

	}
#else
	Serial.println("CV Defaulting Always On Powerup");
	notify_CMD_RESETCONF();
#endif 

	// Not forcing a reset CV Reset to Factory Defaults
	if (DefaultsCVIndex == 0) {
		initDecoder();
	};
}

// the loop function runs over and over again until power down or reset
void loop() {
	
	// Process the RASCII	
	rascii.process(myaddr);

	if (DefaultsCVIndex == 0) {
	// Process the Decoder

		switch (rpmStatus) {
		case stat_Run:

			if (rpmCount >= rpmMAXCount) {
				rpmStop_ms = millis();
				detachInterrupt(0);			//Disable interrupt
				rpmStart = false;
				rpmStop = true;
				rpmCount = 0;
				rpmStatus = stat_Calc;
				lastmillis = millis();
			}
			break;

		case stat_Start:
			rpmStatus = stat_Run;
			rpmStart = true;
			rpmCount = 0;
			// attachInterrupt(0, rpmTacho, RISING); //enable interrupt
			attachInterrupt(0, rpmTacho, RISING); //enable interrupt
			rpmStart_ms = millis();
			break;

		case stat_Idle:
			if (millis() - lastmillis >= 1000) {
				rpmStop = false;
			}
			// minimum 100ms Pause
			if (millis() - lastmillis >= CV_Pause + 1000) {
				rpmStatus = stat_Start;
			}
			break;

		case stat_Calc:
			rpmStatus = stat_Idle;
#ifdef DEBUG_MSG
			// Geschindigkeit = Weg (m) / Zeit (s)

			float _D = (float)CV_Distance / (float)(100); // m
			float _t = (float)(rpmStop_ms - rpmStart_ms) / (float)(1000); // s

			float _v = (float)(_D) / (float)(_t);	// m/s
			float _vcms = (float)(_v) * (float)(100);		// cm/s
			float _vkmh = (float)(_v * 3.6);		// kmh

			Serial.print(F("  Geschwindigkeit: "));
			Serial.print(F("  Vorbild:     "));
			Serial.print(_vcms, 2); Serial.print(F(" cm/s      "));
			Serial.print(_v, 2);    Serial.print(F(" m/s       "));
			Serial.print(_vkmh, 2); Serial.println(F(" kmh       "));

			_vcms = (float)(3.6 * GetCV(103 / 1000) * (float)_vkmh;

			Serial.print(F("  Modell:      "));
			Serial.print(_vcms, 2); Serial.print(F(" cm/s      "));

#endif // DEBUG_MSG
			break;

		}
	}
	else {
		DefaultsCVIndex--; // Decrement first as initially it is the size of the array
		uint8_t cv_ = pgm_read_word(&DefaultCVs[DefaultsCVIndex].CV);
		uint8_t val_ = pgm_read_word(&DefaultCVs[DefaultsCVIndex].Value);

#ifdef DEBUG_MSG
		Serial.print(F("void loop; Set DefaultCV: ")); Serial.print(cv_, DEC); Serial.print(F(" Value: ")); Serial.println(val_, DEC);
#endif 
		SetCV(cv_, val_);

		// Is this the last Default CV to set? if so re-init zDecoder
		if (DefaultsCVIndex == 0) {
			initDecoder();
		}
	}

}

byte entprellzeit = 20;                         // Entprellzeit des Taster. ACHTUNG BYTE nur bis 255 ms !!!
unsigned long lastTime = 0;             // Variable f�r den Timer des Tasters zum entprellen


// this code will be executed every time the interrupt 0 (pin2) gets low.
void rpmTacho() {
	if ((millis() - lastTime) > entprellzeit) {
		rpmCount++;
		lastTime = millis();
	}

}

extern void notify_CMD_INFO() {
	char s[32];
	sprintf(s, "RASCII_Tacho on addr=%d", myaddr);
	rascii.write(s);
}

// Send the configuration to the host.
extern void notify_CMD_GETCONF() {
#ifdef  DEBUG_MSG
	Serial.print(F("notify_CMD_GETCONF"));
#endif
	Serial.print(F("notify_CMD_GETCONF"));
	//byte conf[128];
	//memcpy(conf, &boardSetup, sizeof(boardSetup));
	//rascii.write(CMD_GETCONF, conf, sizeof(boardSetup), RSP);

}

extern void notify_CMD_RESETCONF() {

#ifdef  DEBUG_MSG
	Serial.println(F("void notify_CMD_RESETCONF"));
#endif
	// Make FactoryDefaultCVIndex non-zero and equal to num CV's to be reset 
	// to flag to the loop() function that a reset to Factory Defaults needs to be done
	DefaultsCVIndex = sizeof(DefaultCVs) / sizeof(CVPair);
}

extern void notify_CMD_GETCV(byte addr, byte CVHigh, byte CVLow) {
	byte conf[32];
	char s[100];
	int Index_;
	byte Basis_;


	int CV = (CVHigh * 256) + CVLow + 1;

	conf[0] = addr;
	conf[0] = CVHigh;
	conf[1] = CVLow;
	conf[2] = GetCV(CV);		// Value

	rascii.write(CMD_GETCV, conf, 3, RSP);

#ifdef  DEBUG_MSG
	Serial.print(F("notify_CMD_GETCV; addr: "));
	Serial.print(addr, DEC);
	Serial.print(F(", CV: "));
	Serial.print(CV, DEC);	
	Serial.print(F(", value: "));
	Serial.println(conf[1], DEC);
#endif


}

extern void notify_CMD_SETCV(byte addr, byte CVHigh, byte CVLow, byte value) {
	char s[100];

	int CV = (CVHigh * 256) + CVLow + 1;

	if ((CV == CV_Reset) && (value == CV_Reset)) {
		notify_CMD_RESETCONF();
		sprintf(s, "reset %d = %d", CV, value);
		rascii.write(s);
	}
	else {
		SetCV(CV, value);
		sprintf(s, "set cv %d to %d", CV, value);
		rascii.write(s);
	}

#ifdef  DEBUG_MSG
	Serial.print(F("void notify_CMD_SETCV; addr: "));
	Serial.print(addr, DEC);
	Serial.print(F(", CV: "));
	Serial.print(CV, DEC);
	Serial.print(F(", value: "));
	Serial.println(value, DEC);
#endif
	 

}

extern void read_Digital_Input() {
	byte din[3];

	if (rpmStart != rpmValues[0]) {
		din[0] = myaddr - 1;	// address
		din[1] = 1;			// port, in Rocrail + 1
		din[2] = rpmStart;	// value

#ifdef  DEBUG_MSG
		Serial.print(F("void read_Digital_Input; addr: ")); Serial.print(din[0], DEC);
		Serial.print(F("  Port: ")); Serial.print(din[1], DEC);
		Serial.print(F(", Value: ")); Serial.println(din[2], DEC);
#endif

		rpmValues[0] = rpmStart;
		rascii.write(EVT_DSEN, din, 3, EVT);
	}

	if (rpmStop != rpmValues[1]) {
		din[0] = myaddr - 1;	// address
		din[1] = 2;			// port, in Rocrail + 1
		din[2] = rpmStop;	// value

#ifdef  DEBUG_MSG
		Serial.print(F("void read_Digital_Input; addr: ")); Serial.print(din[0], DEC);
		Serial.print(F("  Port: ")); Serial.print(din[1], DEC);
		Serial.print(F(", Value: ")); Serial.println(din[2], DEC);
#endif

		rpmValues[1] = rpmStop;
		rascii.write(EVT_DSEN, din, 3, EVT);
	}
}

void notify_CVChange(uint8_t CV_, uint8_t value_) {
#ifdef DEBUG_MSG
	Serial.print(F("void notifyCVChange: CV: "));
	Serial.print(CV_, DEC);
	Serial.print(F(" Value: "));
	Serial.println(value_, DEC);
#endif  

	initDecoder();
}
