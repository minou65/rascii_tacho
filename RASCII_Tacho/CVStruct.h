/*
Copyright (c) 2018 andy      <don_funk@bluewin.ch>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created by Andreas Zogg, September 26, 2018.

CVStruct.h - Version 1.0.0
*/

#ifndef _CVSTRUCT_h
#define _CVSTRUCT_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#define Default_Addr	255

#define CV_Addr			1
#define CV_Reset		8

struct CVPair {
	uint8_t CV;
	uint8_t Value;
};

uint8_t DefaultsCVIndex = 0;

const CVPair DefaultCVs[] PROGMEM = {

	// CV1, Board Address
	{ CV_Addr, Default_Addr },

	{100, 65},			// Durchmesser Messrad in 1/10mm Mario Zeller Max Tacho = 6.5mm
	{101, 100},			// Messtrecke in cm
	{102, 10},			// Pause in Sekunden
	{103, 87}			// Masstab

};

#endif

